<?php
$servername = "localhost";  // Change to your MySQL server hostname
$username = "your_username"; // Change to your MySQL username
$password = "your_password"; // Change to your MySQL password
$database = "ltweb";        // Change to the name of your database (ltweb)

$conn = new mysqli($servername, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
	$conn->close();
}
?>
