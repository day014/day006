<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký</title>
    <style>
        .error-text {
            color: red;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

<h2>Form Đăng Ký</h2>

<div id="error-message" class="error-text"></div>

<form id="registration-form">
    <!-- Form inputs as before -->

    <button type="button" onclick="validateForm()">Đăng ký</button>
</form>

<script>
    function validateForm() {
        // Validation code as before

        if ($("#error-message").html() === "") {
            // If all validation passed, redirect to confirm.php
            window.location.href = "confirm.php";
        }

        $('html, body').animate({scrollTop: $("#registration-form").offset().top}, 'slow');
    }
</script>

</body>
</html>
