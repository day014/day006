<?php
include 'database.php'; // Include the database connection

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    // Retrieve data from the POST request
    $fullName = $_POST["fullName"];
    $gender = $_POST["gender"];
    $department = $_POST["department"];
    $dob = $_POST["dob"];
    $address = $_POST["address"];
    $image = $_FILES["image"]["name"];

    // Prepare the SQL insert statement
    $sql = "INSERT INTO students (fullName, gender, department, dob, address, image) VALUES ('$fullName', '$gender', '$department', '$dob', '$address', '$image')";

    // Check if the insertion was successful
    if ($conn->query($sql) === TRUE) {
        echo "Dữ liệu đã được lưu vào cơ sở dữ liệu.";
    } else {
        echo "Lỗi: " . $sql . "<br>" . $conn->error;
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Xác nhận thông tin đăng ký</title>
</head>
<body>

<h2>Thông tin đăng ký của bạn:</h2>

<?php
// Display the registered data
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    echo "Họ và tên: " . htmlspecialchars($fullName) . "<br>";
    echo "Giới tính: " . htmlspecialchars($gender) . "<br>";
    echo "Phân khoa: " . htmlspecialchars($department) . "<br>";
    echo "Ngày sinh: " . htmlspecialchars($dob) . "<br>";
    echo "Địa chỉ: " . htmlspecialchars($address) . "<br>";
    echo "Hình ảnh: " . htmlspecialchars($image) . "<br>";
}
?>

</body>
</html>