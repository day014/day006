-- Create the "ltweb" database
CREATE DATABASE IF NOT EXISTS ltweb;

-- Switch to the "ltweb" database
USE ltweb;

-- Create the "students" table
CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fullName VARCHAR(255) NOT NULL,
    gender ENUM('male', 'female', 'other') NOT NULL,
    department VARCHAR(255) NOT NULL,
    dob DATE NOT NULL,
    address VARCHAR(255),
    image VARCHAR(255)
);
